//
//  TabViewController.swift
//  Reverse_Gear
//
//  Created by nile on 09/04/19.
//  Copyright © 2019 Ayushi. All rights reserved.
//

import UIKit

class TabViewController: UIViewController {

    @IBOutlet weak var heightConstraints: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topHeadingLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
       initialSetUp()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- InitialSetUp
    func initialSetUp() {
        self.navigationController?.navigationBar.isHidden = true
        configureHeight()
        bottomView.center = self.view.center
        bottomView.backgroundColor = UIColor.white
        bottomView.layer.shadowColor = UIColor.lightGray.cgColor
        bottomView.layer.shadowOpacity = 4.0
        bottomView.layer.shadowOffset = CGSize.zero
        bottomView.layer.shadowRadius = 4.0
    }
    
     //MARK:- Bottom View Constrainsts
    func configureHeight()
    {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                heightConstraints.constant = 60
            //  print("iPhone 5 or 5S or 5C")
            case 1334:
                heightConstraints.constant = 60
            //  print("iPhone 6/6S/7/8")
            case 1920, 2208:
                heightConstraints.constant = 60
            //   print("iPhone 6+/6S+/7+/8+")
            case 2436:
                heightConstraints.constant = 80
            //   print("iPhone X, Xs")
            case 2688:
                heightConstraints.constant = 80
            //   print("iPhone Xs Max")
            case 1792:
                heightConstraints.constant = 80
              //  print("iPhone Xr")
            default:
                break
                //   print("unknown")
            }
        }
    }
    
    
    
    @IBAction func commonBtnAction(_ sender: UIButton) {
        
        switch sender.tag {
        case 10:
            break
            
        case 11:
            break
        default:
            break
        }
        
    }
    
    

}
