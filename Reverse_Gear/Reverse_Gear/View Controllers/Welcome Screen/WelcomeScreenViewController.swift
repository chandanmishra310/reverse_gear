//
//  WelcomeScreenViewController.swift
//  Reverse_Gear
//
//  Created by nile on 09/04/19.
//  Copyright © 2019 Ayushi. All rights reserved.
//

import UIKit

class WelcomeScreenViewController: UIViewController {
    
    @IBOutlet weak var signUpBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Initial SetUp
    
    func initialSetUp() {
        navigationController?.navigationBar.isHidden = true
        
    }
    
    @IBAction func commonBtnAction(_ sender: Any) {
        
        view.endEditing(true)
        
        switch (sender as AnyObject).tag {
        case 10:
            
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "logInViewController") as! logInViewController
            navigationController?.pushViewController(newViewController, animated: true)
            break
            
        case 11:
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            navigationController?.pushViewController(newViewController, animated: true)
            
            break
            
        default:
            break
            
        }
        
        
    }
}
