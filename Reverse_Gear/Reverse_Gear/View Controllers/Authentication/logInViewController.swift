//
//  logInViewController.swift
//  Reverse_Gear
//
//  Created by nile on 09/04/19.
//  Copyright © 2019 Ayushi. All rights reserved.
//

import UIKit

class logInViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var passwordBtn: UIButton!
    @IBOutlet weak var passwordImageView: UIImageView!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var passwordTextField: PaddedTextField!
    @IBOutlet weak var emailAddressTextField: PaddedTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        // Do any additional setup after loading the view.
    }
    //MARK:- Initial SetUp
    func initialSetUp() {
        self.hideKeyboardWhenTappedAround()
        navigationController?.navigationBar.isHidden = true
        emailAddressTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        passwordTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor

    }
    
    //MARK:- Buttons Actions
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func commonBtnAction(_ sender: UIButton) {
        view.endEditing(true)
        
        switch sender.tag {
        case 10:
            
            let validationObj = ValidationClass.verifyFieldForLogin(email: self.emailAddressTextField.text!, password: passwordTextField.text!)
            
            if validationObj.1{
                APIManager.apiForLogIn(email: self.emailAddressTextField.text!, password: self.passwordTextField.text!) { (status,message) in
                    if status{
                        AlertController.alert(message: message)
                        let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                        let newViewController = storyboard.instantiateViewController(withIdentifier: "TabViewController") as! TabViewController
                        self.navigationController?.pushViewController(newViewController, animated: true)
                    }
                }
            }
                
            else {
                _ = AlertController.alert(title: "", message: validationObj.0)
            }
            
            break
        case 11:
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
            navigationController?.pushViewController(newViewController, animated: true)
            break
            
        case 12:
            let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let newViewController = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            navigationController?.pushViewController(newViewController, animated: true)
            break
            
        case 13:
            sender.isSelected = !sender.isSelected
            if sender.isSelected == true {
                passwordTextField.isSecureTextEntry = false
                passwordImageView.image = UIImage(named: "eye")
            }
            else {
                passwordTextField.isSecureTextEntry = true
                passwordImageView.image = UIImage(named: "hide")
            }
            break
            
        default:
            break
            
        }
    }
    
    
    
    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 60
            {
                return false
            }
            break
        case 101:
            if str.length > 15
            {
                return false
            }
        default:
            break
        }
        
        //MARK:- Does'nt Support Emojies
        
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next {
            let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
            tf?.becomeFirstResponder()
        }
        else {
            view.endEditing(true)
        }
        return true
    }
}
