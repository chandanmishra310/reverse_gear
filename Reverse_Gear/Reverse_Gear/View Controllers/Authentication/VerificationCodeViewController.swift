//
//  VerificationCodeViewController.swift
//  Reverse_Gear
//
//  Created by nile on 10/04/19.
//  Copyright © 2019 Ayushi. All rights reserved.
//

import UIKit

class VerificationCodeViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var firstOTPTextField: UITextField!
    @IBOutlet weak var secondOTPTextField: UITextField!
    @IBOutlet weak var fifthOtpTextField: UITextField!
    @IBOutlet weak var sixOtpTextField: UITextField!
    @IBOutlet weak var thirdOTPTextField: UITextField!
    @IBOutlet weak var fourthOTPTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    var email = ""
    @IBOutlet weak var newPasswordTextField: PaddedTextField!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var newPasswordImageView: UIImageView!
    var otp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        // Do any additional setup after loading the view.
    }
    //MARK:- Initial SetUp
    func initialSetUp() {
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        emailLbl.text = email
        newPasswordTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        firstOTPTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        secondOTPTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        thirdOTPTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        fourthOTPTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        fifthOtpTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        sixOtpTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
    }
    
    
    //MARK:- Button Action
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func commonBtnAction(_ sender: UIButton) {
        switch sender.tag {
        case 10:
            
            sender.isSelected = !sender.isSelected
            if sender.isSelected == true {
                newPasswordTextField.isSecureTextEntry = false
                newPasswordImageView.image = UIImage(named: "eye")
            }
            else {
                newPasswordTextField.isSecureTextEntry = true
                newPasswordImageView.image = UIImage(named: "hide")
            }
            
            break
            
        case 11:
            
            var otpLength : Int = self.firstOTPTextField.text!.count +  self.secondOTPTextField.text!.count +  self.thirdOTPTextField.text!.count +  self.fourthOTPTextField.text!.count
            otpLength = otpLength + self.fifthOtpTextField.text!.count
            otpLength = otpLength + self.sixOtpTextField.text!.count

            let validationObj = ValidationClass.verifyOTPNPassoword(otp: otpLength, newPassword: newPasswordTextField.text!)
            if validationObj.1{
                
                for i in 0..<6{
                    let field : UITextField = self.view.viewWithTag(i + 1) as! UITextField
                    otp = otp + field.text!
                }
                APIManager.apiForResetPasswordNOTP(email: self.email, otp: self.otp, newPassword: newPasswordTextField.text!) { (status,message) in
                    if status{
                        AlertController.alert(title: "", message: message, buttons: ["OK"], tapBlock: { (alert, index) in
                            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
                        })
                    }
                }
            }
            else {
                _ = AlertController.alert(title: "", message: validationObj.0)
            }
            
            break
        default:
            break
        }
    }
    
    //MARK:- Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        

        switch textField.tag{
        case 100 :
            
            if str.length > 15
            {
                return false
            }
            

            break
        case  1,2,3,4,5,6 :
            
            if textField.tag != 100{
            var str:NSString = textField.text! as NSString
            str = str.replacingCharacters(in: range, with: string) as NSString
            textField.text = string as String
            
            let nextResponder = view.viewWithTag(textField.tag+1)
            let previousResponder = view.viewWithTag(textField.tag-1)
            if !((textField.text?.length)!<1)
            {
                nextResponder?.becomeFirstResponder()
                return false
            } else if (textField.text?.length == 0){
                previousResponder?.becomeFirstResponder()
                return false
            }
            }
            break
            
        default:
            break
        }
        
        return true
    }
}
