//
//  ForgotPasswordViewController.swift
//  Reverse_Gear
//
//  Created by nile on 09/04/19.
//  Copyright © 2019 Ayushi. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var emailAddressTextField: PaddedTextField!
    @IBOutlet weak var submitBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetUp()
        // Do any additional setup after loading the view.
    }
    

    //MARK:- Initial SetUp
    func initialSetUp() {
        self.hideKeyboardWhenTappedAround()
        navigationController?.navigationBar.isHidden = true
        emailAddressTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
   
    }
    
    
    //MARK:- Buttons Actions
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func submitBtnAction(_ sender: UIButton) {

        
        let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
                                let newViewController = storyboard.instantiateViewController(withIdentifier: "VerificationCodeViewController") as! VerificationCodeViewController
                                self.navigationController?.pushViewController(newViewController, animated: true)
        
        
        
//        let validationObj = ValidationClass.verifyFieldForForgotPassword(email: emailAddressTextField.text!)
//
//        if validationObj.1{
//            APIManager.apiForForgotPassword(email: self.emailAddressTextField.text!) { (status,message) in
//                if status{
//                    AlertController.alert(title: "", message: message, buttons: ["OK"], tapBlock: { (alert, index) in
//
//
//                        let storyboard:UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
//                        let newViewController = storyboard.instantiateViewController(withIdentifier: "VerificationCodeViewController") as! VerificationCodeViewController
//                         newViewController.email = self.emailAddressTextField.text!
//                        self.navigationController?.pushViewController(newViewController, animated: true)
//                    })
//                }
//            }
//        }
//        else {
//            _ = AlertController.alert(title: "", message: validationObj.0)
//        }
   }
//
    
    //MARK:- Text Field Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var str:NSString = textField.text! as NSString
        str = str.replacingCharacters(in: range, with: string) as NSString
        switch textField.tag {
        case 100:
            if str.length > 60
            {
                return false
            }
            break
            
        default:
            break
        }
        if textField.isFirstResponder {
            if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                return false
            }
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


