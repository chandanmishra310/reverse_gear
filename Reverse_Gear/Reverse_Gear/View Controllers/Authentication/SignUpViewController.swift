//
//  SignUpViewController.swift
//  Reverse_Gear
//
//  Created by nile on 09/04/19.
//  Copyright © 2019 Ayushi. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var confirmPasswordImageView: UIImageView!
    @IBOutlet weak var passwordImageView: UIImageView!
    @IBOutlet weak var confirmPasswordTextField: PaddedTextField!
    @IBOutlet weak var passwordTextField: PaddedTextField!
    @IBOutlet weak var phoneTextField: PaddedTextField!
    @IBOutlet weak var emailAddressTextField: PaddedTextField!
    @IBOutlet weak var fNameTextField: PaddedTextField!
    @IBOutlet weak var lNameTextField: PaddedTextField!
    
    @IBOutlet weak var signUpBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- Initial SetUp
    func initialSetup() {
        self.hideKeyboardWhenTappedAround()
        navigationController?.navigationBar.isHidden = true
        fNameTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        lNameTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        emailAddressTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        phoneTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        passwordTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
        confirmPasswordTextField.layer.borderColor = UIColor(red: 204/255, green: 215/255, blue: 235/255, alpha: 1.0).cgColor
    }
    
    
    //MARK:- Buttons Actions
    @IBAction func bckBtnAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func commonBtnAction(_ sender: UIButton) {
        switch sender.tag {
        case 10:
            
            let validationObj = ValidationClass.verifyFieldForSignUp(fName: fNameTextField.text!,lName: lNameTextField.text!,email: emailAddressTextField.text!, phone: phoneTextField.text!, password: passwordTextField.text!, confirmPassword: confirmPasswordTextField.text!)
            
            if validationObj.1{
                APIManager.apiForSignUp(fName: fNameTextField.text!, lName: lNameTextField.text!, email: self.emailAddressTextField.text!, phone: self.phoneTextField.text!, password: self.passwordTextField.text!) { (status,message) in
                if status{
                    AlertController.alert(title: "", message: message, buttons: ["OK"], tapBlock: { (alert, index) in
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                }
                
            }
            else {
                _ = AlertController.alert(title: "", message: validationObj.0)
            }
        
        
        break
        case 11:
        navigationController?.popViewController(animated: true)
        break
        
        case 12:
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            passwordTextField.isSecureTextEntry = false
            passwordImageView.image = UIImage(named: "eye")
            
        }
        else {
            passwordTextField.isSecureTextEntry = true
            passwordImageView.image = UIImage(named: "hide")
        }
        
        break
        
        case 13:
        
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            confirmPasswordTextField.isSecureTextEntry = false
            confirmPasswordImageView.image = UIImage(named: "eye")
            
        }
        else {
            confirmPasswordTextField.isSecureTextEntry = true
            confirmPasswordImageView.image = UIImage(named: "hide")
        }
        
        break
        
        default:
        break
    }
}

//MARK:- Text Field Delegate
func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    var str:NSString = textField.text! as NSString
    str = str.replacingCharacters(in: range, with: string) as NSString
    switch textField.tag {
    case 100,101:
        if str.length > 50
        {
            return false
        }
        break
    case 102:
        if str.length > 60
        {
            return false
        }
        
    case 103:
        if str.length > 15
        {
            return false
        }
        
    case 104,105:
        if str.length > 15
        {
            return false
        }
    default:
        break
    }
    
    //MARK:- Does'nt Support Emojies
    if textField.isFirstResponder {
        if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
            return false
        }
    }
    
    return true
}

func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
    if textField.returnKeyType == .next {
        let tf: UITextField? = (view.viewWithTag(textField.tag + 1) as? UITextField)
        tf?.becomeFirstResponder()
    }
    else {
        view.endEditing(true)
    }
    return true
}
}
