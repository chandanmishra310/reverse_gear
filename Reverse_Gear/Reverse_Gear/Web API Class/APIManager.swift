//
//  StaticScreenViewController.swift
//  Zoloh
//
//  Created by Vibhuti Sharma on 14/02/19.
//  Copyright © 2019 nile. All rights reserved.
//

import UIKit

class APIManager: NSObject {
    // MARK: ---  Sign up Api Handler  ---
    class func apiForSignUp(fName:String,lName:String,email : String,phone:String, password : String, completion: @escaping ((Bool,String) -> Void)) {
        
        var userDict : Dictionary = Dictionary<String,AnyObject>()
        userDict["firstName"] = fName as AnyObject
        userDict["lastName"] = lName as AnyObject
        userDict["emailAddress"] = email as AnyObject
        userDict["contactNumber"] = phone as AnyObject
        userDict["password"] = password as AnyObject
       
        ServiceHelper.request(userDict, method: .post, apiName: kSignUpApi, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 201 {
                    let message : String = results.validatedValue("message", expected: "" as AnyObject) as! String
                    completion(true,message)
                    
                } else {

                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,"")
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,"")
            }
        }
    }
    
    
    // MARK: ---  LogInApi Handler  ---
    class func apiForLogIn(email : String,password : String, completion: @escaping ((Bool,String) -> Void)) {
        
        var userDict : Dictionary = Dictionary<String,AnyObject>()
        
        userDict["emailId"] = email as AnyObject
        userDict["password"] = password as AnyObject
        
        ServiceHelper.request(userDict, method: .post, apiName: kLogIn, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let token : String = results.validatedValue("token", expected: "" as AnyObject) as! String
                    completion(true,token)
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,"")
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,"")
            }
        }
    }
    
    // MARK: ---  Forgot Password Handler  ---
    class func apiForForgotPassword(email : String, completion: @escaping ((Bool,String) -> Void)) {
        
        let userDict : Dictionary = Dictionary<String,AnyObject>()
        
        ServiceHelper.request(userDict, method: .get, apiName: kForgotPassword + "/" + email, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let message : String = results.validatedValue("message", expected: "" as AnyObject) as! String
                    completion(true,message)
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,"")
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,"")
            }
        }
    }
    
    
    // MARK: ---  ResetPassword N OTP Handler  ---
    class func apiForResetPasswordNOTP(email : String, otp : String , newPassword : String,completion: @escaping ((Bool,String) -> Void)) {
        
        var userDict : Dictionary = Dictionary<String,AnyObject>()
        
        userDict["emailAddress"] = email as AnyObject
        userDict["otp"] = otp as AnyObject
        userDict["password"] = newPassword as AnyObject

        ServiceHelper.request(userDict, method: .put, apiName: kResetPasswordNOTP, hudType: .default) { (result, error, responseCode) in
            if error == nil {
                let results = result as! Dictionary<String, AnyObject>
                if responseCode == 200 {
                    let message : String = results.validatedValue("message", expected: "" as AnyObject) as! String
                    completion(true,message)
                    
                } else {
                    
                    MessageView.showMessage(message: results["message"] as! String, time: 3.0, verticalAlignment: .bottom)
                    completion(false,"")
                }
            } else {
                MessageView.showMessage(message: (error?.localizedDescription)!, time: 3.0, verticalAlignment: .bottom)
                completion(false,"")
            }
        }
    }
    
    
    
}
