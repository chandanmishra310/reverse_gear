//
//  Utility.swift
//  Zoloh
//  Created by nile on 05/01/19.
//  Copyright © 2018 Nile. All rights reserved.
//

import Foundation
import UIKit
let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
let kWindowWidth = UIScreen.main.bounds.size.width
let kWindowHeight = UIScreen.main.bounds.size.height
func imageToNSString(image: UIImage) -> String {


    let imageData:NSData = image.pngData()! as NSData
    let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
    return (imageStr.length != 0) ? imageStr : ""
}

extension String {
    func deletingPrefix(_ prefix: String) -> String {

        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))

    }
}


