//
//  StaticScreenViewController.swift
//  Zoloh
//
//  Created by Vibhuti Sharma on 14/02/19.
//  Copyright © 2019 nile. All rights reserved.
//

import UIKit

class ValidationClass: NSObject {
    
    // --- email validation message ---
    static let vcErrorEmail                           =  "Please enter email address."
    static let vcErrorValidEmail                      =  "Please enter valid email address."
    static let vcErrorPassword                        =  "Please enter password."
    static let vcErrorValidPassword                   =  "Password must contain at least 5 characters."
    static let vcErrorConfirmPassword                 =  "Please enter confirm password."
    static let vcErrorValidConfirmPassword            =  "Confirm Password must contain at least 5 characters."
    

    // --- name validation message ---
    static let vcErrorFName                            =  "Please enter first name."
    static let vcErrorValidFName                       =  "Please enter valid first name."
   
    
    static let vcErrorLName                            =  "Please enter last name."
    static let vcErrorValidLName                       =  "Please enter valid last name."
    
    
    // --- phone number validation message ---
    static let vcErrorPhoneNumber                     =  "Please enter phone number."
    static let vcErrorValidPhoneNumber                =  "Please enter valid phone number."
    // --- password validation message ---
    static let vcErrorOldPassword                     =  "Please enter old password."
    static let vcErrorValidOldPassword                =  "Old Password must contain at least 5 characters."
    static let vcErroNewPassword                      =  "Please enter new password."
    static let vcErrorValidNewPassword                =  "New password must contain at least 5 characters."
    static let vcErrorMismatchPasswrdAndConPassword   =  "New password and old password must be same."//"Password and confirm password does not match."
    
    // --- Submit a query---
    static let vcErrorSubmitQuery = "Please submit query"
    
    // --- Reset Password
   
    static let vcErrorMismatchNewPasswrdAndConPassword   =  "Confirm password and new password must be same."//"Password and confirm password does not match."
    static let vcErrorMismatchNewPasswrdAndConPasswordForReset   =  "New password and old password can not be same."
    
    static let vcErrorMismatchPasswrdAndConPasswordForReset   =  "Confirm password and password must be same."
    static let vcErrorOTP = "Please enter OTP"
    static let vcErrorValidOTP = "Please enter valid OTP"


    // --- Report Tip
    
     static let vcErrorDate  =  "Please select date."
    static let vcErrorTime  =  "Please select time."
    static let vcErrorTitle  =  "Please enter title."
    static let vcErrorLocation  =  "Please enter location."

    static let vcErrorDescription  =  "Please enter description."

    

    // MARK: --------------------- UIVIEWCONTROLLER SPECIFIC VALIDATION ---------------
    
    // MARK: Login Validation
    class func verifyFieldForLogin(email : String, password : String) -> (String, Bool, Int) {
    
        var verifyObj = (message : "", isVerified : false, index: 0)
        
        // -- verify email/username
        let emailVerification = ValidationClass.verifyEmail(text: email)
        let passwordVerification = ValidationClass.verifyPassword(password: password)
        
        if emailVerification.1 == false {
            verifyObj.message = emailVerification.0
            verifyObj.isVerified = emailVerification.1
            verifyObj.index = 0
        } else if passwordVerification.1 == false {
            verifyObj.message = passwordVerification.0
            verifyObj.isVerified = passwordVerification.1
            verifyObj.index = 1
        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    // MARK: Login Validation


    class func verifyFieldForSignUp(fName : String,lName : String,email : String,phone : String, password : String,confirmPassword : String) -> (String, Bool, Int) {

        var verifyObj = (message : "", isVerified : false, index: 0)

        let firstNameValidation = ValidationClass.verifyfName(text: fName)
        
        let lastNameValidation = ValidationClass.verifylName(text: lName)

        
        let emailVerification = ValidationClass.verifyEmail(text: email)
        let passwordVerification = ValidationClass.verifyPasswordAndConfirmPassword(password: password, confirmPassword: confirmPassword)
        let phoneValidation = ValidationClass.verifyPhoneNumber(text: phone)
        
        if firstNameValidation.1 == false {
            verifyObj.message = firstNameValidation.0
            verifyObj.isVerified = firstNameValidation.1
            verifyObj.index = 0
        }
        else if lastNameValidation.1 == false {
            verifyObj.message = lastNameValidation.0
            verifyObj.isVerified = lastNameValidation.1
            verifyObj.index = 0
        }
        else if emailVerification.1 == false {
            verifyObj.message = emailVerification.0
            verifyObj.isVerified = emailVerification.1
            verifyObj.index = 0
        }
        else if phoneValidation.1 == false {
            verifyObj.message = phoneValidation.0
            verifyObj.isVerified = phoneValidation.1
            verifyObj.index = 0
        } else if passwordVerification.1 == false {
            verifyObj.message = passwordVerification.0
            verifyObj.isVerified = passwordVerification.1
            verifyObj.index = 1
        }
//        else if checkBtn.isSelected == false  {
//            verifyObj.message = "Please accept terms and conditions."
//            verifyObj.isVerified = false
//            verifyObj.index = 1
//        }
        else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }

    // MARK: Forgot Password Validation
    class func verifyFieldForForgotPassword(email : String) -> (String, Bool, Int) {
        var verifyObj = (message : "", isVerified : false, index: 0)
        // -- verify email
        let emailVerification = ValidationClass.verifyEmail(text: email)
        if emailVerification.1 == false {
            verifyObj.message = emailVerification.0
            verifyObj.isVerified = emailVerification.1
            verifyObj.index = 0
        }else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
}

// MARK: ---- extension to define individual verfication methods
extension ValidationClass {
    
    // MARK: ----- email validation -----
    class func verifyEmail(text : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        if text.count == 0 {
            verifyObj.message = vcErrorEmail
        } else if !text.isEmail {
            verifyObj.message = vcErrorValidEmail
        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    // MARK: ----- First name validation -----
    class func verifyfName(text : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        if text.length == 0 {
            verifyObj.message = vcErrorFName
        }
        
            
        else if text.length < 2 {
            verifyObj.message = vcErrorValidFName
        }
        
        else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    
    
    // MARK: ----- Last name validation -----
    class func verifylName(text : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        if text.length == 0 {
            verifyObj.message = vcErrorLName
        }
            
        else if text.length < 2 {
            verifyObj.message = vcErrorValidLName
        }
            
        else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    // MARK: ----- mobile number -----
    class func verifyPhoneNumber(text : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        if text.length == 0 {
            verifyObj.message = vcErrorPhoneNumber
        } else if text.length < 8 { //< 8 { if country code option is also on screen
            verifyObj.message = vcErrorValidPhoneNumber
        } else if text.isContainsAllZeros() {
            verifyObj.message = vcErrorValidPhoneNumber
            
        } else if !text.containsNumberOnly() {
            verifyObj.message = vcErrorValidPhoneNumber
        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }

    // MARK: ----- password -----
    class func verifyPassword(password : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        if password.length == 0 {
            verifyObj.message = vcErrorPassword
            
        } else if password.length < 5 {
            verifyObj.message = vcErrorValidPassword

        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
    
    
    // MARK: ----- Submit Query -----
    class func verifySubmitQuery(submitQuery : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        if submitQuery.length == 0 {
            verifyObj.message = vcErrorSubmitQuery
            
        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }

    // MARK: ----- current password -----
    class func verifyCurrentPassword(password : String) -> (String, Bool) {
        var verifyObj = (message : "", isVerified : false)
        if password.length == 0 {
            verifyObj.message = "Please enter current password."
        } else if password.length < 5 {
            verifyObj.message = "Current password must contains at least 8 characters."
        } else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }

    // MARK: ----- Change old password and New password -----
    class func verifyOldPasswordAndNewPassword(oldpassword : String,newpassword : String, confirmPassword : String) -> (String, Bool, Int) {
        var verifyObj = (message : "", isVerified : false, index: 0)
        
        if oldpassword.length == 0 {
            verifyObj.message = vcErrorOldPassword
            verifyObj.index = 0
        } else if oldpassword.length < 5 {
            verifyObj.message = vcErrorValidOldPassword
            verifyObj.index = 0
        } else if newpassword.length == 0 {
            verifyObj.message = vcErroNewPassword
            verifyObj.index = 1
        }
        else if newpassword.length < 5 {
            verifyObj.message = vcErrorValidNewPassword
            verifyObj.index = 1
        }
            
        else if newpassword == oldpassword {
            verifyObj.message = vcErrorMismatchNewPasswrdAndConPasswordForReset
            verifyObj.index = 1
        }
            
        else if confirmPassword.length == 0 {
            verifyObj.message = vcErrorConfirmPassword
            verifyObj.index = 1
        }
            
        else if confirmPassword.length < 5 {
            verifyObj.message = vcErrorValidConfirmPassword
            verifyObj.index = 1
        }
            
        else if confirmPassword != newpassword {
            verifyObj.message = vcErrorMismatchNewPasswrdAndConPassword
            verifyObj.index = 1
        }
        else {
            verifyObj.isVerified = true
        }
        
        return verifyObj
    }
    
    // MARK: -----  password and Confirm password -----
    class func verifyPasswordAndConfirmPassword(password : String, confirmPassword : String) -> (String, Bool, Int) {
        var verifyObj = (message : "", isVerified : false, index: 0)
        
        if password.length == 0 {
            verifyObj.message = vcErrorPassword
            verifyObj.index = 0
        } else if password.length < 5 {
            verifyObj.message = vcErrorValidPassword
            verifyObj.index = 0
        }
        else if confirmPassword.length == 0 {
            verifyObj.message = vcErrorConfirmPassword
            verifyObj.index = 1
        }
            
        else if confirmPassword.length < 5 {
            verifyObj.message = vcErrorValidConfirmPassword
            verifyObj.index = 1
        }
            
        else if confirmPassword != password {
            verifyObj.message = vcErrorMismatchPasswrdAndConPasswordForReset
            verifyObj.index = 1
        }
            
        else {
            verifyObj.isVerified = true
        }
        
        return verifyObj
    }
    
    // MARK: ----- Report View  -----
    class func verifyReportView(date: UIButton, time : UIButton, title: String ,location : String, description: String) -> (String, Bool, Int) {
        var verifyObj = (message : "", isVerified : false, index: 0)
        
        
        
        if date.currentTitle == "MM/DD/YYYY" {
            verifyObj.message = vcErrorDate
                        verifyObj.index = 0
        }
        
        
        else if time.currentTitle == "HH:MM:SS" {
            verifyObj.message = vcErrorTime
            verifyObj.index = 0
        }
            
            
   
        else if title.length == 0 {
            verifyObj.message = vcErrorTitle
            verifyObj.index = 0
        }
            
        else if location.length == 0 {
            verifyObj.message = vcErrorLocation
            verifyObj.index = 0
            
        }
            
        else if description.length == 0 {
            verifyObj.message = vcErrorDescription
            verifyObj.index = 0
        }
            
         else {
            verifyObj.isVerified = true
        }
        
        return verifyObj
    }
    
    
    class func verifyOTPNPassoword(otp : Int,newPassword : String) -> (String, Bool,Int) {
        var verifyObj = (message : "", isVerified : false, index: 0)
        if otp == 0 {
            verifyObj.message = vcErrorOTP
            verifyObj.index = 0
            verifyObj.isVerified = false
        }
        else if otp != 6{
            verifyObj.message = vcErrorValidOTP
            verifyObj.index = 0
            verifyObj.isVerified = false
        }

        else  if newPassword.length == 0 {
            verifyObj.message = vcErroNewPassword
            verifyObj.index = 0
            verifyObj.isVerified = false

        }
      else if newPassword.length < 5 {
            verifyObj.message = vcErrorValidNewPassword
            verifyObj.index = 1
            verifyObj.isVerified = false

        }
        else {
            verifyObj.isVerified = true
        }
        return verifyObj
    }
  
}
